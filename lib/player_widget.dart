import 'player_model.dart';
import 'size_config.dart';
import 'package:flutter/material.dart';

class PlayerWidget extends StatefulWidget {
  final PlayerModel player;

  PlayerWidget(PlayerModel player) : player = player;

  @override
  State<StatefulWidget> createState() {
    return _PlayerWidgetState();
  }
}

class _PlayerWidgetState extends State<PlayerWidget> {
  void playerChangeSex() {
    setState(widget.player.changeSex);
  }

  void playerIncGear() {
    setState(widget.player.gearInc);
  }

  void playerDecGear() {
    setState(widget.player.gearDec);
  }

  void playerIncLevel() {
    setState(widget.player.levelInc);
  }

  void playerDecLevel() {
    setState(widget.player.levelDec);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig s = SizeConfig(context);
    double scaler = s.safeBlockVertical / 6;
    final double iconSize = scaler * 48.0;
    final double nameFontSize = scaler * 20.0;
    final double levelFontSize = scaler * 40.0;
    final double numberFontSize = scaler * 30.0;
    final double labelFontSize = scaler * 16.0;

    return Center(
      child: Card(
        color: widget.player.level == 10 ? Colors.lightGreen : Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(
              left: 16.0, right: 16.0, top: 12.0, bottom: 12.0),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(widget.player.name,
                      style: TextStyle(
                          fontSize: nameFontSize, fontWeight: FontWeight.bold)),
                ),
                Text('Strength',
                    style: TextStyle(
                        fontSize: labelFontSize, fontWeight: FontWeight.bold)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(widget.player.strength.toString(),
                      style: TextStyle(
                          fontSize: levelFontSize,
                          fontWeight: FontWeight.bold)),
                ),
                IconButton(
                    icon: Icon(widget.player.sex == Sex.male
                        ? Icons.male
                        : Icons.female),
                    onPressed: playerChangeSex),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Row(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: <Widget>[
                        Text('Level',
                            style: TextStyle(
                                fontSize: labelFontSize,
                                fontWeight: FontWeight.bold)),
                        IconButton(
                            icon: Icon(Icons.arrow_drop_up),
                            iconSize: iconSize,
                            onPressed: playerIncLevel),
                        Text(widget.player.level.toString(),
                            style: TextStyle(
                                fontSize: numberFontSize,
                                fontWeight: FontWeight.bold)),
                        IconButton(
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: iconSize,
                            onPressed: playerDecLevel),
                      ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: <Widget>[
                        Text('Gear',
                            style: TextStyle(
                                fontSize: labelFontSize,
                                fontWeight: FontWeight.bold)),
                        IconButton(
                            icon: Icon(Icons.arrow_drop_up),
                            iconSize: iconSize,
                            onPressed: playerIncGear),
                        Text(widget.player.gear.toString(),
                            style: TextStyle(
                                fontSize: numberFontSize,
                                fontWeight: FontWeight.bold)),
                        IconButton(
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: iconSize,
                          onPressed: playerDecGear,
                        ),
                      ]),
                    )
                  ]),
                ),
                ElevatedButton(
                  focusNode: FocusNode(skipTraversal: true),
                  child: Text('Reset'),
                  onPressed: () {
                    setState(widget.player.resetPlayer);
                  },
                )
              ]),
        ),
      ),
    );
  }
}
