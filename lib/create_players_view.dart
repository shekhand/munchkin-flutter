import 'package:flutter/material.dart';

import 'game_view.dart';

class CreatePlayersView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const int number_of_fields = 6;
    List<TextEditingController> controllers = [];
    List<Widget> widgetsList = [];

    widgetsList.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        'Create the game',
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ));

    for (int i = 0; i < number_of_fields; i++) {
      controllers.add(TextEditingController());
      widgetsList.add(TextField(
        controller: controllers[i],
        decoration: InputDecoration(labelText: 'Player ${i + 1}'),
        maxLength: 20,
      ));
    }

    widgetsList.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        ElevatedButton(
            child: Text('Clear',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            onPressed: () {
              for (TextEditingController controller in controllers) {
                controller.clear();
              }
            }),
        ElevatedButton(
          child: Text('Create',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          onPressed: () {
            List<String> playerNames = [];
            for (var controller in controllers) {
              String name = controller.text;
              if (name.isNotEmpty) {
                playerNames.add(name);
              }
            }
            if (playerNames.isNotEmpty) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => GameView(playerNames)),
              );
            }
          },
        ),
      ],
    ));

    return Scaffold(
        appBar: AppBar(
          title: Text('Munchkins'),
        ),
        body: Center(
          child: Container(
            width: 400.0,
            height: 600.0,
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView(
                  children: widgetsList,
                ),
              ),
            ),
          ),
        ));
  }
}
