import 'package:flutter/widgets.dart';

class SizeConfig {
 late MediaQueryData _mediaQueryData; 
 late double screenWidth;
 late double screenHeight;
 late double blockSizeHorizontal;
 late double blockSizeVertical;
 
 late double _safeAreaHorizontal;
 late double _safeAreaVertical;
 late double safeBlockHorizontal;
 late double safeBlockVertical;
 
 SizeConfig(BuildContext context) {
    this._mediaQueryData = MediaQuery.of(context);
    this.screenWidth = _mediaQueryData.size.width;
    this.screenHeight = _mediaQueryData.size.height;
    this.blockSizeHorizontal = screenWidth / 100;
    this.blockSizeVertical = screenHeight / 100;

    this._safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    this._safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    this.safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    this.safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}