enum Sex { male, female }

class PlayerModel {
  String name;
  int level = 0;
  int gear = 0;
  Sex sex = Sex.male;
  

  int get strength {
    return gear + level;
  }

  void levelInc() {
    level = level == 10 ? level : level + 1;
  }

  void levelDec() {
    level = level == 1 ? level : level - 1;
  }

  void gearInc() {
    gear++;
  }

  void gearDec() {
    gear = gear == 0 ? gear : gear - 1;
  }

  void resetPlayer() {
    level = 1;
    gear = 0;
  }

  void changeSex() {
    sex = sex == Sex.male ? Sex.female : Sex.male;
  }
  
  PlayerModel(String name): name=name {
    this.level = 1;
    this.gear = 0;
  }
}