import 'package:munchkin_flutter/player_model.dart';
import 'package:munchkin_flutter/info_widget.dart';

import 'player_widget.dart';
import 'package:flutter/material.dart';

class GameView extends StatelessWidget {
  final List<String> playerNames;

  GameView(List<String> playerNames) : playerNames = playerNames;

  @override
  Widget build(BuildContext context) {
    List<PlayerWidget> list = [];
    for (String name in playerNames) {
      list.add(PlayerWidget(PlayerModel(name)));
    }

    return Scaffold(
        appBar: AppBar(title: Text('Munchkins')),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.help),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => InfoWidget()));
          },
        ),
        body: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: list));
  }
}
